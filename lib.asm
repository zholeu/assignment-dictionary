%define SYS_EXIT 60
%define SYS_WRITE 1
%define stdin 0
%define stdout 1
%define stderr 2

section .text 
global exit
global string_length
global print_char
global print_newline
global print_string
global print_error
global print_uint
global print_int
global string_equals
global parse_uint
global parse_int
global read_word
global string_copy


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall                         

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax                    
.loop:
    cmp byte [rdi+rax], 0          
    je .end
    inc rax                        
    jmp .loop
.end:
    ret                             

print_string_error:
    mov rdx, stderr
    jmp printer

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rdx, stdout
.printer:
    mov rsi, rdi
    push rsi
    push rdx
    call string_length
    pop rdi
    pop rsi
    mov rdx, rax      
    mov rax, SYS_WRITE        
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi                        
    mov rdi, rsp                   
    call print_string               
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:                        
    mov rax, rdi
    mov rdi, rsp
    push stdin
    sub rsp, 16
    dec rdi
    mov r8, 10
.loop:
    xor rdx, rdx
    div r8
    or dl, 0x30
    dec rdi
    mov [rdi], dl
    test rax, rax
    jnz .loop
    call print_string
    add rsp, 24    
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns print_uint                              
    push rdi
    mov rdi, '-'   
    call print_char                 
    pop rdi
    neg rdi 
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte [rdi]
    cmp al, byte [rsi]  
    jne .different
    inc rdi                             
    inc rsi
    test al, al
    jnz string_equals
    mov rax, 1
    ret
.different:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r13 
    push r12
    xor r12, r12 
    xor r13, r13
    mov r13, rdi
.space:
    call read_char 
    cmp rax, 0     
    je .success      
    cmp rax, 0x20
    je .space
    cmp rax, 0x9
    je .space
    cmp rax, 0xA
    je .space
.word:
    cmp rsi, r12
    je .overbuf
    mov byte[r13 + r12], al
    inc r12
    call read_char
    cmp rax, 0
    je .success
    cmp rax, 0x9
    je .success
    cmp rax, 0x20
    je .success
    cmp rax, 0xA
    je .success
    jmp .word
.success:
    xor rax, rax
    mov byte[r13 + r12], al
    mov rax, r13
    mov rdx, r12
    jmp .end
.overbuf:
    xor rax, rax
.end:
    pop r12 
    pop r13
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor r9, r9
    mov r8, 10
.loop:
    mov sil, byte[rdi+r9]
    test sil, sil
    jz .end
    cmp sil, 48
    jb .end
    cmp sil, 57
    ja .end
    sub sil, 48
    inc r9
    mul r8
    add rax, rsi
    jmp .loop
.end:
    mov rdx, r9
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось 
parse_int:
    xor rax, rax
    xor rsi, rsi
    mov sil, byte[rdi]
    cmp sil, '-'
    je .signed
    call parse_uint
    ret
.signed:
    inc rdi
    call parse_uint
    test rdx, rdx
    jnz .end
    ret 
.end:
    inc rdx
    neg rax
    ret
    
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length 
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jae .overbuf
    push rsi
.loop:
    mov dl, byte [rdi]
    mov byte [rsi], dl
    inc rdi
    inc rsi
    test dl, dl
    jnz .loop
    pop rax
    ret   
.overbuf:
    xor rax, rax                        
    ret
