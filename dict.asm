%define key_size 8

extern string_equals

section .text

global find_word

find_word:
    .loop:
        test rsi, rsi
        jz .not_found
        push rdi
        push rsi
        add rsi, key_size
        call string_equals
        pop rsi
        pop rdi
        test rax, rax
        jnz .found
        mov rsi, [rsi] 
        jmp .loop
    .not_found:
        xor rax, rax
        jmp .end
    .found:
        mov rax, rsi
        jmp .end   
    .end:
        ret
