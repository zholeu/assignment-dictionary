%include 'colon.inc'

%define key_size 8

%macro add_key 1
%if key_size = 8
    db %1, 0
%elif key_size = 16
    dw %1, 0
%else
    dd %1, 0
%endif
%endmacro

global list_start
global list_end
    
list_start:

colon "hello", key_1
add_key "Hello"

colon "cat", key_2
add_key "Cat"

colon "dog", key_3
add_key "Dog"

list_end:
