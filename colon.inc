%define last 0

%macro colon 2
%%last: dq last 
db %1, 0
label %+ %2:

%define last %%last
%endmacro
